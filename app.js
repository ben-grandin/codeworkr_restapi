const express = require("express"),
	logger = require("morgan"),
	bodyParser = require("body-parser"),
	mongoose = require("mongoose"),
	helmet = require("helmet"),
	// Routes
	users = require("./routes/users"),
	cars = require("./routes/cars");

mongoose.connect("mongodb://localhost/express", { useNewUrlParser: true });

const app = express();

app.use(helmet());

// Middlewares
app.use(logger("dev"));
app.use(bodyParser.json());

// Routes
app.use("/users", users);
app.use("/cars", cars);

// Catch 404 errors and forward them to error handler
app.use((req, res, next) => {
	const err = new Error("Not found");
	err.status = 404;
	next(err);
});

// Error handler function
app.use((err, req, res, next) => {
	const error = app.get("env") === "development" ? err : {};
	const status = err.status || 500;
	// Respond to client
	res.status(status).json({
		error: {
			message: error.message
		}
	});

	// Respond to ourselves
	console.error(err);
});

// Start the server
const port = app.get("port") || 3000;
app.listen(port, () => console.log(`Server is listening on port ${port}`));
