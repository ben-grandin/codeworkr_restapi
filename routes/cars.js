const router = require("express-promise-router")(),
	CarsController = require("../controllers/cars.js"),
	{
		validateParams,
		validateBody,
		schemas
	} = require("../helpers/routeHelpers.js");

router
	.route("/")
	.get(CarsController.index)
	.post(validateBody(schemas.carSchema), CarsController.newCar);

router
	.route("/:carId")
	.get(validateParams(schemas.idSchema, "carId"), CarsController.getCar)
	.put(
		[
			validateParams(schemas.idSchema, "carId"),
			validateBody(schemas.putCarSchema)
		],
		CarsController.replaceCar
	)
	.patch([
		validateParams(schemas.idSchema, "carId"),
		validateBody(schemas.patchCarSchema)
	],
	CarsController.updateCar)
	.delete(validateParams(schemas.idSchema, "carId"), CarsController.deleteCar );
module.exports = router;
